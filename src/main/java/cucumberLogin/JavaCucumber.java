package cucumberLogin;

import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class JavaCucumber {
public static ChromeDriver driver;

	
	
	@Given("Login URL")
	public void LoginURL() {
		
		driver.get("http://leaftaps.com/opentaps");
				
	}
	
	@And("Enter the Username as a (.*)")
	public void LoginUsername(String data) {
		driver.findElementById("username").sendKeys(data);
		
		}	
	
	@And("Enter the Password as a (.*)")
	public void LoginPass(String data) {
		driver.findElementById("password").sendKeys(data);
		
		}	
	
	@When("User clicks the login button")
	public void LoginClick() {
		driver.findElementByClassName("decorativeSubmit").click();

		}	
	
	@Then("Close the Browser")
	public void Loginclose() {
		driver.close();

		}
		
		
	
}
