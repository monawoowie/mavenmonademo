package reports;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnReports {
		
	public static void main(String[] args) throws IOException {
	
		//File level
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		//TestCase Level
		ExtentTest suite = extent.createTest("TC001_CreateLead", "Create a new Lead");
		suite.assignAuthor("mona");
		suite.assignCategory("smoke");
		
		ExtentTest test = suite.createNode("Leads");
		
		//Steps Level
		test.pass("The data DemoSalesManager entered successfully"
				,MediaEntityBuilder.createScreenCaptureFromPath("./1.jpeg").build());
		test.pass("The data crmsfa entered successfully"
				,MediaEntityBuilder.createScreenCaptureFromPath("./reports/1.jpg").build());
		test.pass("The login button clicked successfully"
				,MediaEntityBuilder.createScreenCaptureFromPath("./reports/1.jpg").build());
		
		//end 
		extent.flush();
		
		System.out.println("hai");
		
		
		
		
	}

}
