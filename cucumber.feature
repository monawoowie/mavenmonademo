Feature: General Login 

Background: 
	Given Login URL 
	
Scenario Outline: With a username & Password 
	And Enter the Username as a <username> 
	And Enter the Password as a <password> 
	When User clicks the login button 
	Then Close the Browser 
	
	Examples: 
		|username|password|
		|DemoSalesManager|crmsfa|
		|DemoCSR|crmsfa|
		
		
Scenario: With a username & Password 
	And Enter the Username as a DemoSales123 
	And Enter the Password as a crmsfa 
	When User clicks the login button 
	But Close the Browser 
